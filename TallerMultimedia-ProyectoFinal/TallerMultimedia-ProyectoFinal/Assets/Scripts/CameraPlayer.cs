﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayer : MonoBehaviour {

	public GameObject player;

	void FixedUpdate() {
		this.transform.localPosition = new Vector3 (player.transform.localPosition.x, this.transform.localPosition.y, this.transform.localPosition.z);
	}
}